const express = require('express')
const uuid = require('uuid')

const app = express()

app.use(express.json())

const baseMusicas = []

function cadastrarMusica(nome) {
    for (let index in baseMusicas) {
        if (baseMusicas[index].nome === nome) {
            return "Musica já cadastrada!"
        }
    }
    
    let musicaNova = {
        id: uuid.v4(),
        nome: nome,
        avaliacao: 0
    }
    baseMusicas.push(musicaNova)
    return musicaNova
}

function listarMusicas() {
    return baseMusicas
}

function avaliarMusica(nome, avaliacao) {
    for (let index in baseMusicas) {
        if (baseMusicas[index].nome === nome) {
            baseMusicas[index].avaliacao = avaliacao
            return baseMusicas[index]
        }
    }
    return "Musica não existe!"
}


app.post('/cadastrar', (req, res) => {
    musicaCadatrada = cadastrarMusica(req.query.nome)
    res.status(200).send(musicaCadatrada)
})

app.get('/listar', (req, res) => {
    musicas = listarMusicas()
    res.status(200).send(musicas)
})

app.post('/avaliar', (req, res) => {
    musicaAvaliada = avaliarMusica(req.query.nome, req.query.avaliacao)
    res.status(200).send(musicaAvaliada)
})

app.listen(4200, async () => {
    console.log("Avaliação de Musicas. Porta 4200")
})


